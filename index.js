(function(angular) {
  'use strict';
angular.module('myApp', []).controller('MainCtrl', function MainCtrl($scope, $http) {
  
  // this.menu = {
  //   id: Math.random().toString(36).substr(2, 10),
 	// 	name: "Great & Healthy Food",
  //   items: [{
  //     id: Math.random().toString(36).substr(2, 10),
  //     name: "Appetizers",
  //     items: [{
  //       id: Math.random().toString(36).substr(2, 10),
  //       name: "Homemade dumplings",
  //       items: [{
  //         id: Math.random().toString(36).substr(2, 10),
  //         name: "Meat",
  //         isNew: true,
  //         isGlutenFree: true,
  //         items: [{
  //           id: Math.random().toString(36).substr(2, 10),
  //           name: "Small (4)",
  //           price: 3.85
  //         },{
  //           id: Math.random().toString(36).substr(2, 10),
  //           name: "Medium (8)",
  //           price: 6.45
  //         }]
  //       }]
  //     }]
  //   }]
  // };

  this.getMenu = () => {
    let config = {
        headers : {
            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
        }
    }
    
    $http.post("getMenu.php", null, config)
    .then(function successCallback(response) {
      $scope.ctrl.menu = response.data;
    }, function errorCallback(response) {
      alert(response.data);
      console.log(response);
    });

  }

  this.getMenu();

  this.save = () => {
    let config = {
        headers : {
            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
        }
    }
    
    $http.post("process.php", this.menu, config)
    .then(
      function(response){
        // success callback
        console.log(response);
        alert(response.data);
      }, function(response){
        // failure callback
        console.log(response);
        alert(response.data);
      }
    );
  }

  this.reset = () => {
    if (confirm('Clear menu?')) {
        this.menu = {
          name: "",
          items: []
        }
    }
  }

});
})(window.angular);