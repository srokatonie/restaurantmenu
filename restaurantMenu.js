(function(angular) {
	'use strict';
	function RestaurantMenuController($scope, $timeout, $http) {

		// initialize menu object
		this.$onInit = () => { this.menu = $scope.$ctrl.menu }

		this.showOutput = false;

		let fields = {
			'default': {
				id: null,
				name: "",
				items: []
			},
			'priceOption': {
				id: null,
				name: "",
				price: null
			},
		}

		this.addRow = (obj, addType) => {
			let field = angular.copy(fields[addType]);
			// generate id which is required to remember show/hide 
			// when moving up/down
			field.id = Math.random().toString(36).substr(2, 10);
			obj.items.push(field);
		}

		this.removeRow = (obj, index) => {
			if (confirm('Delete "' + obj.items[index].name + '"?')) {
			     obj.items.splice(index, 1);
			}
		}

		this.move = (where, obj, index) => {
			console.log(obj);
			// to move up index > 0
			if ((where == 'up' && index > 0) || 
				(where == 'down' && index < obj.items.length-1)) {
				
				let current = angular.copy(obj.items[index]);

				// delete current index
				obj.items.splice(index, 1);

				// move index
				if (where == 'up') {
					obj.items.splice(index-1, 0, current);
				} else if (where == 'down') {
					obj.items.splice(index+1, 0, current);
				}
			} else {
				alert("Can't move futher " + where);
			}
		}

	}

	angular.module('myApp').component('restaurantMenu', {
		templateUrl: 'RestaurantMenu.html',
		controller: RestaurantMenuController,
		bindings: {
			menu: '='
		}
	});
})(window.angular);